import express from 'express';
//const express = require('express'); //Se requiere el modulo de express
import morgan from 'morgan';
//const morgan = require('morgan');
import cors from 'cors';
//const cors = require('cors');
import path from 'path';
import mongoose from 'mongoose';
import router from './routes';


//Conexion a la base de datos
mongoose.Promise = global.Promise;
const dbUrl = 'mongodb://localhost:27017/dbsistema'
mongoose.connect(dbUrl, { useCreateIndex: true, useNewUrlParser: true, })
    .then(mongoose => console.log('Conectado a la bd'))
    .catch(err => console.log(err));


const app = express();  //Se instancia el modulo de express


//middlewares
app.use(morgan('dev'));
app.use(cors());

//Recibir peticiones json desde  metodo post
app.use(express.json());  //Se habilita para enviar y recibir 
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));  //Se configuran los archivos estaticos o publicos.

//Establecemos el uso de la ruta, el objeto router gestiona las rutas 
app.use('/api',router);

//Configuracion del puerto
app.set('port', process.env.PORT || 3000); //Se toma el puerto asignado para el servicio



app.listen(app.get('port'), () => {
    console.log('Servidor ejecutandose en el puerto ' + app.get('port'));
    console.log(path.join(__dirname, 'public'));
});


